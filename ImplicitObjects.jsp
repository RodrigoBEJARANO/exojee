<%@page import="java.util.Enumeration" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Implicit Objects</title>
</head>
<body>
<h1>REQUEST</h1>

<%
Enumeration<String> headers = request.getHeaderNames();
while(headers.hasMoreElements()){
	String nom = headers.nextElement();
	out.println(nom + " : " + request.getHeader(nom) + "<br />" );
}

//https://www.tutorialspoint.com/jsp/jsp_client_request.htm

%>

<h2>Segunda prueba</h2>

Protocole implant� para le Server: <% out.println(request.getProtocol()); %> <br />
Protocole utilis� para le client: <% out.println(request.getScheme()); %> <br />
Nom du serveur: <% out.println(request.getServerName()); %> <br />
Port du serveur: <% out.println(request.getServerPort()); %> <br />
Adresse IP du  serveur<% out.println(request.getLocalAddr()); %> <br />
Adresse IP du  client<% out.println(request.getRemoteAddr()); %> <br />
Methode HTTP: <% out.println(request.getMethod()); %> <br />
Le nom de ce script est: <% out.println(request.getRequestURL()); %> <br />
Le r�pertoire java est: <% out.println(request.getContextPath()); %> <br />

</body>
</html>